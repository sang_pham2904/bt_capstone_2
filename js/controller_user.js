function render(sp) {
  let sanPham = "";
  for (let i = 0; i < sp.length; i++) {
    sanPham += `<div class="col mb-5">
    <div class="card h-100">
      
      <img
        class="card-img-top"
        src="${sp[i].img}"
        alt="..."
      />
     
      <div class="card-body p-4">
        <div class="text-center">
          
          <h5 class="fw-bolder">${sp[i].name}</h5>
          
          $${sp[i].price}
        </div>
        <div>${sp[i].desc}</div>
      <div>Camera trước : ${sp[i].frontCamera}</div>
      <div>Camera sau : ${sp[i].backCamera}</div>
      <div>Màn hình : ${sp[i].screen}</div>
      <div>Loại điện thoại: ${sp[i].type}</div>
      </div>
      
      <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
        <div class="text-center">
          <a class="btn btn-outline-dark mt-auto" onclick="themSP('${sp[i].id}')"
            >Add to cart</a
          >
        </div>
      </div>
    </div>
  </div>`;
  }
  document.getElementById("main").innerHTML = sanPham;
}

function renderGioHang(sp) {
  let sanPham = "";
  let tongTien = 0;
  for (let i = 0; i < sp.length; i++) {
    sanPham += `<tr class = "py-2"><td>
          
          
            <img
              class="img_giohang"
              src="${sp[i].img}"
            /></td>
            <td>${sp[i].name}</td>
            <td class="quanty">
              <button onclick = "giamSoLuong(${
                sp[i].id
              })" class ="btn btn-warning">-</button>
              <span class = "Quantity px-2"> ${sp[i].quantity} </span>
              <button onclick ="tangSoLuong(${
                sp[i].id
              })" class ="btn btn-warning">+</button>
            </td>
            <td>$ ${(sp[i].price).toLocaleString()}</td>
            <td>$ ${(sp[i].tinhTien()).toLocaleString()}</td>
            <td><button class = "btn" onclick = "xoaSPGioHang(${
              sp[i].id
            })"><i class="fa-solid fa-trash-can"></i></button></td>
            </tr>
            `;
    tongTien += sp[i].tinhTien();
  }
  document.getElementById("cart_item").innerHTML = sanPham;
  document.getElementById("Tinh_tien").innerHTML = tongTien.toLocaleString();
}
function toastify(mes) {
  Toastify({
    text: mes,
    duration: 3000,
    destination: null,
    newWindow: true,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(160deg, #0093E9 0%, #80D0C7 100%)",
    },
    onClick: function () {}, // Callback after click
  }).showToast();
}

function renderSoLuong() {
  axios({
    url: `${Base_URL}/user`,
    method: "GET",
  })
    .then((res) => {
      let soLuong = res.data.reduce((tsl, item, index) => {
        return (tsl += item.quantity * 1);
      }, 0);
      document.getElementById("soLuong").innerHTML = soLuong;
    })
    .catch((err) => {
      console.log(err);
    });
}
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
